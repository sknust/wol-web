import sys
import yaml
import mariadb
import socket
import re

if len(sys.argv) != 2:
  print("Usage: python import-dhcpd.py dhcpd_config_file")
  exit(1)

with open('settings.yml') as file:
  try:
    config = yaml.safe_load(file)
  except yaml.YAMLError as exception:
    print(exception)

dbconfig = {
  'host': config['db']['host'],
  'user': config['db']['user'],
  'password': config['db']['password'],
  'database': config['db']['db']
}

myconn = mariadb.connect(**dbconfig)
mycursor = myconn.cursor(dictionary=True)

with open(sys.argv[1], 'r') as file:
  lines = file.readlines()

data = []
inhostblock = 0

for l in lines:
  if inhostblock:

    x = re.search('^\s*hardware\s+ethernet\s+([0-9a-fA-F:]*)\s*;', l)
    if x:
      ether = x.group(1)
    
    x = re.search('^\s*fixed-address\s+(.*);', l)
    if x:
      try:
        sr = socket.getaddrinfo(x.group(1), None, family=socket.AF_INET, flags=socket.AI_CANONNAME)
        name = sr[0][3]
        ip = sr[0][4][0]
      except:
        # Name lookup failed. This entry is obsolete
        print("Failed to resolve host name ", x.group(1))
        inhostblock = 0

    # If we have a } in the line without a previous comment or {, we end the host block
    if re.search('^[^#;{]*}', l):
      inhostblock = 0
      if name != '' and ip != '' and ether != '':
        data.append((name, ip, ether))
        print((name, ip, ether))
      name = ''
      ip = ''
      ether = ''
  else:
    # Search for any whitespace followed by "host" followed by anything that's not initiating a comment and a {
    if re.search('^\s*host[^#;]*{', l):
      inhostblock = 1


mycursor.executemany(
  "INSERT INTO hosts (hostname, ip, mac) VALUES (%s, INET_ATON(%s), UNHEX(REPLACE(%s, ':', '')))",
  data
)
myconn.commit()