# WOL Web

## Description
Simple web interface for Wake on LAN for both direct WOL broadcast as well as broadcast via proxy.

This applications expects a database with name, ip and MAC address of all devices. It does not support authentication natively, configure your reverse proxy for this.

## Requirements
* Webserver with authentication (i.e. Apache httpd, nginx)
* Either wsgi support for the webserver or a dedicated wsgi/Flask compatible server and reverse proxy configuration in the web server
* Access to a MariaDB database with support for functions as default values (i.e. >=MariaDB 10.2.1)
* python3

As well as the following Python modules:
* flask
* mariadb
* ipaddress
* yaml
* socket


## Installation
* Clone this repository
* Copy `settings.yml.example` to `settings.yml` and edit as required
* Place logo in `/static/logo.png`
* Initialise your MySQL/MariaDB database with `db-init.sql` and populate it with values.
* Set up your webserver and optionally dedicated wsgi server.

## Sample Apache httpd configuration with mod_wsgi, mod_ssl and mod_ldap
Create a `wolweb.wsgi` alongside `wolweb.py` with this content, adjusting the path as necessary:
~~~
#!/usr/bin/python
import sys
sys.path.insert(0,"/srv/wol-web/")
from wolweb import app as application
~~~

Adjust `/etc/httpd/conf.d/ssl.conf` or similar like this:
~~~
WSGIDaemonProcess wol user=wol threads=5
WSGIScriptAlias / /srv/wol-web/wolweb.wsgi
Alias /static /srv/wol-web/static

<Directory /srv/wol-web>
        WSGIProcessGroup wol
        WSGIApplicationGroup %{GLOBAL}
        AuthType basic
        AuthName "Access with LDAP account"
        AuthBasicProvider ldap
        AuthLDAPURL "ldap://ldap-server1.fqdn ldap-server2.fqdn/cn=base,dc=dn,dc=for,dc=users"
        Require valid-user
</Directory>

<Location />
        AuthType basic
        AuthName "Access with LDAP account"
        AuthBasicProvider ldap
        AuthLDAPURL "ldap://ldap-server1.fqdn ldap-server2.fqdn/cn=base,dc=dn,dc=for,dc=users"
        Require valid-user
</Location>

SSLCertificateFile /etc/ssl/certs/ssl-cert.pem
SSLCertificateKeyFile /etc/ssl/certs/ssl-key.pem
SSLCertificateChainFile /etc/ssl/certs/ssl-fullchain.pem
~~~

## Proxy support
For waking up clients outside the local network (i.e. different VLAN), this tool supports the following types of WOL proxies (to be configured in `settings.yml`):
* `mikrotik`: Uses the WOL tool built-into Mikrotik RouterOS >= 3.23. Requires passwordless SSH access to the Mikrotik device and at least the `ssh` and `test` permission for the selected user. See https://wiki.mikrotik.com/wiki/Use_SSH_to_execute_commands_(public/private_key_login) for how to set up public key SSH authentication with RouterOS devices
* `ssh-wakeonlan`: Requires passwordless SSH access to a machine with `wakeonlan` installed.

## Data population support script
`import-dhcpd.py` is a quick and dirty import script which grabs host information from a dhcpd static configuration file. Usage is `python import-dhcpd.py /path/to/dhcpd/config/file`. It expects each host to have exactly one `hardware ethernet` and one `fixed-address` entry.

## Logging
It is recommended to log the requests to `/wake` including URL parameters and authenticated user to be able to track abuse. Calls to that page are performed with a (base64) UUID parameter. On the database server, the following query can be used to detect the affected host:

`SELECT hostname, INET_NTOA(ip) AS ip FROM hosts WHERE id = CONV(HEX(FROM_BASE64('base64-string-from-log')), 16, 10);`

To prevent enumeration of all hosts, consider placing rate limits on `/search`.

## Support
Feel free to contact phyadmin@physik.uni-bielefeld.de for best-effort support. Alternatively, open a ticket in Gitlab.

## Contributing
I am open to contributions. Please submit pull requests.

## Authors and acknowledgment
* Sebastian Knust

## License
This project is licensed under Creative Commons Attribution-ShareAlike 4.0 International (https://creativecommons.org/licenses/by-sa/4.0/).

Includes jQuery and jQuery UI (c) jQuery Foundation and other contributors, licensed under MIT license. See `/static/jquery-ui-1.13.2.custom/LICENSE.txt` for full license.


