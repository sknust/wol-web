from flask import Flask, render_template, jsonify, request
import mariadb
from flask.json import JSONEncoder
import ipaddress
import yaml
import socket

# for remote Wake via ssh
import os

with open('settings.yml') as file:
  try:
    config = yaml.safe_load(file)
  except yaml.YAMLError as exception:
    print(exception)


class MyJSONEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, int):
            return str(o)
        return super().default(o)

class MyFlask(Flask):
    json_encoder = MyJSONEncoder

app = MyFlask(__name__)

dbconfig = {
  'host': config['db']['host'],
  'user': config['db']['user'],
  'password': config['db']['password'],
  'database': config['db']['db']
}



###################
# Wake-Up Helpers #
###################

# Wake up in local VLAN
def local_wake(mac):
  # The WOL magic packet consists of 6 times 0xFF followed by 16 repetitions of the MAC address
  magic = b'\xff' * 6 + mac * 16

  # Prepare socket
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

  # Send raw packet
  s.sendto(magic, ('<broadcast>', 7))

# Wake up in remote VLAN via Mikrotik
def mikrotik_wake(proxy, user, mac):
  os.system('ssh '+user+'@'+proxy+' tool wol mac=' + mac.hex())

# Wake up in remote VLAN via SSH + wakeonlan command line programm
def ssh_wakeonlan(proxy, user, mac):
  s=mac.hex()
  os.system('ssh '+user+'@'+proxy+' wakeonlan ' + ':'.join(s[i:i+2] for i in range(0,len(s),2)))


##########
# Webapp #
##########

# Index page
@app.route("/")
def index(warn=""):
  return render_template('index.html', warn=warn)


# Hostname search functionality, returns JSON incl. host ID
@app.route("/search")
def search():
  term = request.args.get('term', default = '', type = str)
  if len(term)<2 or "%" in term:
    return jsonify(())
  
  term += "%"

  mycursor = mariadb.connect(**dbconfig).cursor(dictionary=True)
  mycursor.execute('SELECT TO_BASE64(UNHEX(HEX(id))) AS id, hostname AS value FROM hosts WHERE hostname LIKE %s LIMIT 10', (term,))
  return jsonify(mycursor.fetchall())


# Wake host specified by ID
@app.route("/wake")
def wake():
  uuid = request.args.get('uuid', default = '', type = str)
  if uuid == '':
    return index("Please select a target host from the drop down menu!")

  mycursor = mariadb.connect(**dbconfig).cursor(dictionary=True)
  mycursor.execute('SELECT hostname, INET_NTOA(ip) AS ip, mac FROM hosts WHERE id = CONV(HEX(FROM_BASE64(%s)),16,10)', (uuid,))
  data = mycursor.fetchall()[0]
  
  # Determine helper function based on target host IP and proxy configuration
  try:
    c = next(p for p in config['proxy'] if ipaddress.ip_address(data['ip']) in ipaddress.ip_network(p['subnet']))
    if c['type'] == 'mikrotik':
      mikrotik_wake(c['host'], c['user'], data['mac'])
      return render_template('wake.html', hostname=(data['hostname']+' via Mikrotik proxy'))
    if c['type'] == 'ssh-wakeonlan':
      ssh_wakeonlan(c['host'], c['user'], data['mac'])
      return render_template('wake.html', hostname=(data['hostname']+' via SSH proxy'))
  except:
    # No proxy found
    local_wake(data['mac'])

  return render_template('wake.html', hostname=data['hostname'])
